# Desafio NG7 - Resposta
`Sabemos que a tomada de decisão, em qualquer área de negócio, é algo de extrema importância se você quiser que a sua empresa esteja em um lugar de destaque, por isso a diretoria acabou de solicitar à sua equipe para desenvolver um painel de dashboard que traga as informações dos possíveis clientes, para a expansão dos negócios da empresa em outros países:`

 * Clientes por gênero;
 * Clientes por idade;
 * Quais países possuem mais possíveis clientes;
 * Quais das mulheres possuem mais de 18 anos.

## Getting started
Primeiro, foi escolhido o framework ViteJS para uso da biblioteca React na sua versão em Typescript. Os arquivos foram estruturados em pastas seguindo uma adpatção do padrão MVC com componentização funcional.

### Antes de iniciar o projeto você precisa instalar as dependências
npm i ou yarn i

### Em seguida, para inciar o projeto, usado o comando:
npm run dev





## Dificuldades e desafios que tive